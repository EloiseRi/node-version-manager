"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeVersionManager = void 0;
const mongoose_1 = require("mongoose");
const node_fetch_1 = __importDefault(require("node-fetch"));
const https_1 = __importDefault(require("https"));
const fs_1 = __importDefault(require("fs"));
// check TimeStamp
const nodeVersionsSchema = new mongoose_1.Schema({
    lastCheck: { type: Date, required: true },
    latestVersion: { type: String, required: true },
    addedVersions: (Array)
});
const nodeVersions = (0, mongoose_1.model)('NodeVersions', nodeVersionsSchema);
class NodeVersionManager {
    constructor() {
        this.connectToDb = () => __awaiter(this, void 0, void 0, function* () {
            yield (0, mongoose_1.connect)('mongodb+srv://eloise:wbce@cluster0.penctgz.mongodb.net/?retryWrites=true&w=majority');
            console.log('Connected.');
        });
        this.connectToDb();
    }
    retrieveNodeVersions() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield (0, node_fetch_1.default)('https://nodejs.org/dist/index.json');
            const data = yield response.json();
            data.forEach((e) => {
                // This RegEx parse file to keep only LTS (even number) versions from 10 to 18.
                const regexp = new RegExp(/v[12][02468].*/);
                if (regexp.test(e.version)) {
                    this.findAllDownloadableFiles(`https://nodejs.org/download/release/${e.version}/SHASUMS256.txt`, e.version);
                }
                if (e.version.match(/v[12][02468].*/)) {
                    console.log("Matched !");
                }
            });
        });
    }
    findAllDownloadableFiles(fileUrl, nodeVersion) {
        https_1.default.get(fileUrl, (res) => {
            if (res.statusCode != 200) {
                console.log("non-200 response status code:", res.statusCode);
                console.log("for url:", fileUrl);
                return;
            }
            if (res.statusCode == 200) {
                res.setEncoding('utf-8');
                res.on('data', (body) => {
                    const files = body.split('\n');
                    files.forEach((line) => {
                        // line contain [SHA256 hash][whitespace][path/to/file]
                        // the next line trim it to keep only the path/to/file
                        const curatedLink = line.split(' ')[2];
                        if (curatedLink !== undefined) {
                            let subfolders = curatedLink.split('/');
                            subfolders.pop(); // Remove the last element, which isn't a subfolder but the actual file to download
                            this.downloadVersionPackage(`https://nodejs.org/download/release/${nodeVersion}/${curatedLink}`, nodeVersion, subfolders, `${nodeVersion}/${curatedLink}`);
                        }
                    });
                });
            }
        });
    }
    downloadVersionPackage(fileUrl, downloadFolder, subfolders, downloadPath) {
        if (!fs_1.default.existsSync(downloadFolder)) {
            fs_1.default.mkdirSync(`./${downloadFolder}`);
        }
        // Create a loop make the folder and its subfolders in the right order, no matter how many sub there are.
        if (subfolders.length > 0) {
            let updatedFolderPath = `./${downloadFolder}`;
            subfolders.forEach((subfolder) => {
                updatedFolderPath = updatedFolderPath.concat(`/${subfolder}`);
                if (!fs_1.default.existsSync(updatedFolderPath)) {
                    fs_1.default.mkdirSync(updatedFolderPath);
                }
            });
        }
        https_1.default.get(fileUrl, (res) => {
            if (res.statusCode != 200) {
                console.log("Error on:", fileUrl);
                console.log("Response status code:", res.statusCode);
                return;
            }
            const writeStream = fs_1.default.createWriteStream(downloadPath);
            res.pipe(writeStream);
            writeStream.on("finish", () => {
                writeStream.close();
                console.log("Download Completed");
            });
        });
    }
    run() {
        this.retrieveNodeVersions();
        // Check if document in DB is empty ?
        // If empty dl all needed files
        // Else compare addVersions to json file from nodejs.org
    }
}
exports.NodeVersionManager = NodeVersionManager;
