import { Schema, model, connect } from 'mongoose';
import fetch from 'node-fetch';
import https from 'https';
import fs, { WriteStream } from 'fs';

interface INodeVersions {
  lastCheck: Date,
  latestVersion: string,
  addedVersions: Array<string>
}
// check TimeStamp

const nodeVersionsSchema = new Schema<INodeVersions>({
  lastCheck: { type: Date, required: true },
  latestVersion: { type: String, required: true},
  addedVersions: Array<String>
})

const nodeVersions = model<INodeVersions>('NodeVersions', nodeVersionsSchema);

export class NodeVersionManager {
  constructor() {
    this.connectToDb();
  }

  private connectToDb = async () => {
    await connect('mongodb+srv://eloise:wbce@cluster0.penctgz.mongodb.net/?retryWrites=true&w=majority')
    console.log('Connected.')
  }

  async retrieveNodeVersions() {
    const response = await fetch('https://nodejs.org/dist/index.json')
    const data: Array<any> = await response.json();

    data.forEach((e) => {
      // This RegEx parse file to keep only LTS (even number) versions from 10 to 18.
      if (e.version.match(/v[12][02468].*/)) {
        this.findAllDownloadableFiles(`https://nodejs.org/download/release/${e.version}/SHASUMS256.txt`, e.version)
      }
    })
  }

  findAllDownloadableFiles(fileUrl: string, nodeVersion: string) {
    https.get(fileUrl, (res) => {
      if (res.statusCode != 200) {
        console.log("non-200 response status code:", res.statusCode);
        console.log("for url:", fileUrl);
        return;
      }
  
      if (res.statusCode == 200) {
        res.setEncoding('utf-8')
        res.on('data', (body) => {
          const files = body.split('\n')
          files.forEach((line: string) => {
            // line contain [SHA256 hash][whitespace][path/to/file]
            // the next line trim it to keep only the path/to/file
            const curatedLink = line.split(' ')[2] 
            if (curatedLink !== undefined) {
              let subfolders = curatedLink.split('/')
              subfolders.pop() // Remove the last element, which isn't a subfolder but the actual file to download
              this.downloadVersionPackage(`https://nodejs.org/download/release/${nodeVersion}/${curatedLink}`, 
                                                          nodeVersion, subfolders, `${nodeVersion}/${curatedLink}`)
            }
          })
        })
      }
    })
  }

  downloadVersionPackage(fileUrl: string, downloadFolder: string, subfolders: Array<string>, downloadPath: string) {
    if (!fs.existsSync(downloadFolder)) {
      fs.mkdirSync(`./${downloadFolder}`)
    }

    // Create a loop make the folder and its subfolders in the right order, no matter how many sub there are.
    if (subfolders.length > 0) {
      let updatedFolderPath = `./${downloadFolder}`
      subfolders.forEach((subfolder) => {
        updatedFolderPath = updatedFolderPath.concat(`/${subfolder}`)
        if (!fs.existsSync(updatedFolderPath)) {
          fs.mkdirSync(updatedFolderPath)
        }
      })
    }

    https.get(fileUrl, (res) => {
      if (res.statusCode != 200) {
        console.log("Error on:", fileUrl)
        console.log("Response status code:", res.statusCode);
        return;
      }

      const writeStream = fs.createWriteStream(downloadPath)
      res.pipe(writeStream);

      writeStream.on("finish", () => {
        writeStream.close();
        console.log("Download Completed");
      });
    })
  }

  run() {
    this.retrieveNodeVersions();
    // Check if document in DB is empty ?
    // If empty dl all needed files
    // Else compare addVersions to json file from nodejs.org
  }

}